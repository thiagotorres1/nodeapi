const http = require("http");
const express = require("express");
const app = express();
const cep = require('cep-promise');

app.get("/", function(req, res) {
    let cep = req.params.cep
    res.send(`<h1>Servidor ExpressJS </h1>`);
});

app.get("/cep", function(req, res) {
    res.send(`<h1>Insita um Cep na URL</h1>`);
});

app.get("/cep/:requestcep", function(req, res) {

    let cepParaConsulta= req.params.requestcep
    
    cep(cepParaConsulta)
      .then((result) => {
        // retorno dos dados em caso de success
        return res.json({ 
            'cep':result.cep,
            'logradouro': result.street,
            'bairro': result.neighborhood,
            'cidade': result.city,
            'uf': result.state 
        });
      })
      .catch((error) => {
        // retorno do erro caso exista
        return res.status(400).json(error);
      });
});

http.createServer(app).listen(3000, () => console.log("Servidor rodando local na porta 3000"));