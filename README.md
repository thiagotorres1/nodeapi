# Servidor-nodejs
Exemplo de um projeto para criar um servidor com ExpressJS e NodeJS

Para executar o projeto, tenha o node instalado em seu pc e rode os seguintes comandos:

## 1 - node install;
## 2 - node servidor.js;
## 3 - com o servidor rodando acesse 'http://localhost:3000/cep/[#######]' onde [#######] corresponde ao cep desejado;
